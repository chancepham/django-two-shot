from django import forms
from .models import Receipt,ExpenseCategory,Account


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ['name', 'number']
class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = ['vendor', 'total', 'tax', 'date', 'category', 'account']
        widgets = {
            'date': forms.DateInput(attrs={'type': 'date'}),
        }

class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ['name']
