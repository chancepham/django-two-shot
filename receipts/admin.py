from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register the ExpenseCategory model
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner')
    search_fields = ('name', 'owner__username')

# Register the Account model
@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('name', 'number', 'owner')
    search_fields = ('name', 'number', 'owner__username')

# Register the Receipt model
@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = ('vendor', 'total', 'tax', 'date', 'purchaser', 'category', 'account')
    search_fields = ('vendor', 'purchaser__username', 'category__name', 'account__name')
    list_filter = ('date', 'purchaser', 'category', 'account')
