from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150, label='Username')
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
        label='Password'
    )
class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150, label='Username')
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
        label='Password'
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
        label='Confirm Password'
    )

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_confirmation = cleaned_data.get("password_confirmation")

        if password and password_confirmation and password != password_confirmation:
            self.add_error('password_confirmation', "Passwords do not match.")
        return cleaned_data
