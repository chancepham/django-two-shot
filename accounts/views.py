from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login,logout
from django.urls import reverse
from .forms import LoginForm,SignUpForm
from django.contrib.auth.models import User
def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')  # Replace 'some_view' with the name of the view you want to redirect to after login
            else:
                # Return an 'invalid login' error message.
                form.add_error(None, 'Invalid username or password')
    else:
        form = LoginForm()

    return render(request, 'accounts/login.html', {'form': form})
def logout_view(request):
    logout(request)
    return redirect(reverse('login'))
def sign_up_view(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = User.objects.create_user(username=username, password=password)
            user.save()
            login(request, user)
            return redirect('home')  # Redirect to some view after successful sign-up
    else:
        form = SignUpForm()

    return render(request, 'accounts/signup.html', {'form': form})
