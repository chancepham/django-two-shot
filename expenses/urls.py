

from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView
urlpatterns = [
    path("admin/", admin.site.urls),
    path('', RedirectView.as_view(url='/receipts/', permanent=False), name='home'),
    path('receipts/', include('receipts.urls')),  # Include the receipts app URLs
    path('accounts/', include('accounts.urls')),
]
#therewego
