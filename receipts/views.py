from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from .models import Receipt,Account,ExpenseCategory
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm

@login_required
def receipt_list_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(request, 'receipts/receipts.html', {'receipts': receipts})
@login_required
def create_receipt_view(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')  # Redirect to the home page after creation
    else:
        form = ReceiptForm()

    return render(request, 'receipts/create_receipt.html', {'form': form})
@login_required
def category_list_view(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    category_data = []
    for category in categories:
        num_receipts = Receipt.objects.filter(category=category).count()
        category_data.append({'name': category.name, 'num_receipts': num_receipts})
    return render(request, 'receipts/category_list.html', {'categories': category_data})

@login_required
def account_list_view(request):
    accounts = Account.objects.filter(owner=request.user)
    account_data = []
    for account in accounts:
        num_receipts = Receipt.objects.filter(account=account).count()
        account_data.append({'name': account.name, 'number': account.number, 'num_receipts': num_receipts})
    return render(request, 'receipts/account_list.html', {'accounts': account_data})
@login_required
def create_category_view(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect('category_list')  # Redirect to the list of categories after creation
    else:
        form = ExpenseCategoryForm()

    return render(request, 'receipts/create_category.html', {'form': form})
@login_required
def create_account_view(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect('account_list')  # Redirect to the list of accounts after creation
    else:
        form = AccountForm()

    return render(request, 'receipts/create_account.html', {'form': form})
